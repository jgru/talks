* A Conversation with Org
/Document, develop and configure with =org-babel=/

** Intro
*** tl;dr
=org-babel= enables the execution of source code within org-mode buffers.

Use cases:
- Literate programming [cite:@knuth1984]
  + Reproducible research [cite:@schulte2012]
  + Talks/lectures/...
- Configuration as code [cite:@rahman2018]
- ...and many more use cases.

*** A Question to Begin With

/Who/ uses =org-babel= on a regular basis?

*** Disclaimer
- Content of the talk:
  - Based on my personal usage and preferences
  - Not an exhaustive guide
  - Multiple self-contained examples to illustrate variety
  - Everything covered here (and more) at:
    https://orgmode.org/worg/org-contrib/babel/intro.html

- Target audience:
  - Emacs users (obviously...)
  - Some experience with [[https://orgmode.org/][org-mode]]

** Motivation
*** Let's Start with A Famous Quote...
#+begin_quote
Instead of imagining that our main task is to instruct a computer what
to do, let us *concentrate* rather *on explaining to human beings* what we
want a computer to do.
#+end_quote -- [cite:@knuth1984 p. 97]

*** A Multi-Language Computing Environment
The idea behind =org-babel=:
#+begin_example
                                  org-mode
                                +--------------------------------+
                                |                                |
                                | * Markup                       |
                                |   - prose                      |
   Source code                  |   - code composition           |                HTML
   run.c                        |   - data analysis              |              +---------------------+
  +---------------+             |                                |              |                     |
  |               |             | #+begin_src C :tangle run.c    |              |                     |
  | int main(){   |             |    int main(){                 |              |         LaTeX       |
  |   return 0;   |    tangle   |      return 0;                 |    weave     |      +--------------+----------+
  | }             +<------------+    }                           +------------->+      |                         |
  |               |             | #+end_src                      |              |      | \section{Markup}        |
  |               |             |                                |              |      | \begin{itemize}         |
  +---------------+             |                                |              +------+  \item prose            |
                                | #+headers: :results graphics   |                     |  \item code composition |
                                | #+begin_src R :file fig.pdf    |                     |  \item data analysis    |
                                |  plot(data)                    |                     | \end{itemize}           |
                                | #+end_src                      |                     +-------------------------+
                                |                                |
                                |                                |
                                +-------------+----+-------------+
                                              ^     \
                                            -/       \-
                              Raw output,  /           \    Embedded data
                            tabular data, /             \   and source code
                                 figures, |             |   in arbitrary
                                     etc. \             /   languages
                                           \           /
                                            -\       /-
                                              -------

                                          Code Evaluation
#+end_example
Taken from [cite:@schulte2012]

** An Overview
*** Why the Name?
_Biblical story:_
People strive to build a tower tall enough to reach heaven and become
god-like. God sabotages this endeavour /"and there confound[s] their
language, that they may not understand one another's speech"/ (Gen
11,7,8 [fn:1])

_Analogy:_
=org-babel= is about letting many different languages work
together [fn:2].

*** Talk numbers...
- More than \(120\) languages/programmable facilities supported
  - \(> 24\) langs distributed with =org-contrib= [fn:3]
- Included as part of Org (>7.0)

*** What is it? How Does It Look Like?
=org-babel= augments Org code blocks by providing:

- Interactive and programmatic execution of code blocks
- Code blocks as functions that accept parameters
- Export to files for literate programming [fn:4]

#+begin_src elisp
(print "Hello org-babel!")
#+end_src

#+RESULTS:
: Hello org-babel!

*** Basic Configuration

- Activate the [[https://orgmode.org/worg/org-contrib/babel/languages/index.html][languages]] you use:
#+begin_src elisp :eval never
(setq org-babel-load-languages
      '((emacs-lisp . t)
        (python     . t)
        (shell      . t)
        (<lang>     . t)))

(org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
#+end_src

*** Convenience Configuration
- =org-src-fontify-natively= :: Syntax highlighting
- =org-src-tab-acts-natively= :: Use TAB-behaviour from lang's major mode
- =org-confirm-babel-evaluate= :: Disable annoying confirmations
- =org-babel-no-eval-on-ctrl-c-ctrl-c= :: (Don't) Exec on =C-c C-c=

*** Apply configuration

#+begin_src elisp :results silent
(setq org-src-fontify-natively t
      org-src-tab-acts-natively t
      org-confirm-babel-evaluate nil
      org-babel-no-eval-on-ctrl-c-ctrl-c nil)
#+end_src

** Concepts and Usage
*** Code Block Insertion
- Use =org-insert-structure-template= (=C-c C-,=) to insert a source block
- Use =org-edit-src-code= (=C-c '=)

#+begin_example
,#+begin_src <language> <switches> <header arguments>
<body>
,#+end_src
#+end_example

*** Code Block Execution
- Execute and print results via =C-c C-c=
  - Function =org-babel-execute-src-block=
- Open results in separate buffer via =C-c C-o=
  - Function =org-open-at-point=

*** Switches and Header Args

- <language>        :: identifier for lang to use
- <switches>        :: control exec, export and format
- <header arguments :: control many facets (tangling, vars,...)
- <body>            :: code to exec

*** Results of Evaluation
Result handling depends on header-args:
- collection :: Define how results are gathered
- type       :: which type of result will the code block return
- handling   :: how will insertion work
- format     :: how will the result be "formatted"[fn:5]

**** Result Collection
- Define how the results should be collected
- Two fundamentally different modes:
  + =:results value= -> functional mode
  + =:results output= -> scripting mode.

**** Result Type
- what result types to expect from the execution
- automatically determine result type by default
- Alternative:
  - =verbatim=, =table=, =file=, =scalar=,...

***** Tabular Example
#+begin_src python :results value table
t = [["a", 1], ["b", 2], ["c", 3]]
return t
#+end_src

#+RESULTS:
| a | 1 |
| b | 2 |
| c | 3 |

***** File Example
#+begin_src shell :results output file :file "./demo-files/demo.txt" :mkdirp yes
echo "Hello org-babel"
#+end_src

#+RESULTS:
[[file:./demo-files/demo.txt]]

Optionally, specify =:output-dir /some/dir=

**** Result Handling
- How will insertion work
  - =replace= :: Remove previous result
  - =append=  :: append result to existing one
  - =silent=  :: output nothing
  - ...

#+begin_src shell :results output append
echo "Hello org-babel from M-x Erlangen"
#+end_src

#+RESULTS:
: Hello org-babel
: Hello org-babel from M-x Erlangen

*** Pass Arguments to Code Block
**** Specify Variable One by One
- Define =:var=:
#+begin_src shell :var DEV="vboxnet0" :results output verbatim
ip link show $DEV
#+end_src

#+RESULTS:
: 6: vboxnet0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP mode DEFAULT group default qlen 1000
:     link/ether 0a:00:27:00:00:00 brd ff:ff:ff:ff:ff:ff

**** Specify Variables as a Table

***** Tabular Data in Python
#+name: data_table
| x | 3 |
| y | 2 |

#+begin_src python :var val=2 data=data_table
d = dict((x, y) for x, y in data)
return d['x'] * d['y'] * val
#+end_src

#+RESULTS:
: 12

***** Tabular Data in Elisp

#+name: hosts
| Hostname    |          IP |
|-------------+-------------|
| vpc-123456a | 172.16.1.11 |
| vpc-123456a | 172.16.2.12 |
| vpc-123456a | 172.16.3.13 |

#+begin_src elisp :var hosts=hosts :results file :file ./demo-files/hosts.org :mkdirp yes
(let ((lines "")
      (props nil))
      ;; process the table
      (mapc #'(lambda (row)
            (let ((hostname (car row))
                  (ip (car (cdr row))))
              ;; Construct a property drawer
              (setq props (format ":PROPERTIES:\n:IP: %s\n:END:\n" ip))
              ;; Add the headline to lines
              (setq lines (concat lines (format "* =%s=\n%s" hostname props)))))
            hosts)
      ;; return string and write to new org-file
      lines)
#+end_src

#+RESULTS:
[[file:./demo-files/hosts.org]]

***** Tabular Data in the Shell
/A bit finicky with =shell=, for examples see [fn:6]/

#+name: files
| Filename | Password | Data   |
|----------+----------+--------|
| A        | pass123  | data   |
| B        | weak     | chunks |

#+begin_src shell :var TBL=files :results output verbatim
while read row
do
    FILE=$(echo "${row}" | cut -f1)
      PW=$(echo "${row}" | cut -f2)
    DATA=$(echo "${row}" | cut -f3)
    echo ${DATA} > ${FILE}
    gpg --symmetric --batch --yes --passphrase ${PW} ${FILE}
    rm ${FILE}
done <<< $TBL

ls -al *.gpg
#+end_src

#+RESULTS:
: -rw-r--r-- 1 user01 user01 76 Mar  1 08:07 A.gpg
: -rw-r--r-- 1 user01 user01 78 Mar  1 08:07 B.gpg

**** Passing results
Use =#+name= to pass the computed results from one code block...

#+name: block-1
#+begin_src elisp :results value
(current-time-string)
#+end_src

#+RESULTS: block-1
: Tue Mar  1 08:08:33 2022

...to another
#+begin_src elisp :var input=block-1 :results value
(let ((m))
  (setq m (when (string-match "\\([0-9]\\{2\\}:[0-9]\\{2\\}\\)" input)
              (match-string 1 input)))
  (concat "It is " m))
#+end_src

#+RESULTS:
: It is 08:09

*** Sessions
- Specify =:session=-header argument
- Spawn interactive session as an "inferior process" for ...
  + Python, R, Ruby and shell
#+begin_src python :session a :results silent
x = 3
#+end_src
then
#+begin_src python :session a :results output verbatim
print(x)
#+end_src

#+RESULTS:
: 3

- Attach to session via =org-babel-pop-to-session= (useful to kill it)

*** Dependent source blocks
- Formulate dependencies
- Using the rather old =noweb=-syntax [fn:7]
  - reference another chunk name by placing its name in =<<...>>=
  - described at detail in [cite:@johnson1997]

**** =noweb=-Syntax
#+NAME: setup
#+BEGIN_SRC emacs-lisp
(setq flag "4d2d782045726c616e67656e")
#+END_SRC

#+RESULTS: setup
: 4d2d782045726c616e67656e

#+BEGIN_SRC emacs-lisp :noweb yes
<<setup>>
(let ((i 0)
      (l (length flag)) s)
      (while (< i l)
         (setq s (concat s (format "%c" (read (concat "#x" (substring flag i (+ i 2)))))))
         (setq i (+ i 2)))
      (progn s))
#+END_SRC

#+RESULTS:
: M-x Erlangen

**** =noweb=-Header Arguments
:PROPERTIES:
:header-args: :noweb-ref locate-srv-by-domain
:END:

Let's assume, we need to locate a server by its domain name...

First, retrieve the IP to domain:
#+BEGIN_SRC sh
ip=$(dig +short $domain)
#+END_SRC

Query a geo database:
#+BEGIN_SRC sh
geoiplookup $ip | cut -d':' -f2
#+END_SRC

**** Reference These Codeblocks

#+BEGIN_SRC sh :noweb yes
domain="forensi.cs.fau.de"
<<locate-srv-by-domain>>
#+END_SRC

#+RESULTS:
| DE | Germany |

*** Inline code
Without header args: src_<lang>{<code>} or with header args:

src_<lang>[<args>]{<code>}, for example src_python[:session a]{x*2}
{{{results(=6=)}}}, where =x= is a variable existing in the python
session [fn:8].

*** Tangling
- Literate programming docs are "woven"
- Code has to be tangled for execution
- Use header args:
  + =:tangle yes=    :: specifies to tangle to automatically chosen filename
  + =:tangle <FILE>= :: specifies the specified file to "tangle to"

**** Tangling Manually
- Use =org-babel-tangle=, bound to =C-c C-v t=, to do so
- Example [[file:demo-files/tangle-example.org]]

#+begin_src elisp :results value silent
(org-babel-tangle-file "tangle-example.org")
#+end_src

#+RESULTS:

- Automatically done by
  =(org-babel-load-file FILE &optional COMPILE)=
  for literate configs

**** Auto Tangling
Always tangle a specific file on save:

#+begin_src elisp :results verbatim
(defun jgru/auto-tangle-example ()
  (when (string-equal (buffer-file-name)
                      (expand-file-name "./tangle-example.org"))
        (let ((org-confirm-babel-evaluatenil))
          (org-babel-tangle))))

(add-hook 'org-mode-hook
          (lambda () (add-hook 'after-save-hook #'jgru/auto-tangle-example)))
#+end_src

Taken from [fn:9]

*** Exporting/Weaving
- Use standard export-dispatcher [fn:10]
- Control export behaviour of with the header args:
  - =:exports= to control, which part is included in the woven doc
    + =code=, =results=, =both= or =none=
  - =:eval= to control, whether code will be executed on export
    - Personal preference: =no-export= or =query-export=
**** Example 1: LaTeX

[[file:demo-files/export-tex-example.org]]

**** Example 2: Technical Docu
Export technical docu

[[file:export-presentation.org]]

with the help of [[https://readthedocs.org/][readthedocs]] [fn:11].

**** Example 3: E-Mail
Use =org-babel= when responding to mail in =mu4e= with the help of
=org-msg=.

** Further Selected Topics
*** Asynchronous Execution
- [[https://github.com/astahlman/ob-async][=ob-async=]] introduces a new header-arg =:async=
- creates a new Emacs process with =org-babel=-functions [fn:12]

#+begin_src shell :async
sleep 3 && echo "Done!"
#+end_src

#+RESULTS:
: Done!

*** Remote Execution
- Use /Transparent Remote Access, Multiple Protocols/ (TRAMP) with =org-babel=
- To evaluate a block on a remote machine via TRAMP, set the =:dir= header
  value appropriately with [[https://www.gnu.org/software/tramp/#File-name-syntax-1][tramp syntax]] (see [fn:13])

=> Convenient way to administer remote machines (...kind of /CaaC/)

**** Run Commands on a Remote Machine
- =:dir /ssh:username@host#port:/some/dir=
  - port is optional here

#+begin_src shell :dir /ssh:user01@localhost#22:~
uname -a
#+end_src

#+RESULTS:
: Linux zero 5.15.0-2-amd64 #1 SMP Debian 5.15.5-2 (2021-12-18) x86_64 GNU/Linux

**** Run Commands Locally with Sudo
- Use =:dir /sudo::~= to elevate your privs
- Tip: create a =yas-snippet= for the following skeleton:

#+begin_src shell :dir (concat "/sudo::" (expand-file-name "."))
whoami
#+end_src

#+RESULTS:
: root

*** =lsp-org=
- *Very experimental* support for running the language servers
  inside of source blocks
- obtains informationblock from the source block header
  - specify =<lang>= + =:tangle=
- Start via =lsp-org= [fn:14]

#+begin_src python :tangle tmp.py :results output verbatim
x = 10
for i in range(10):
    print(i * x)
#+end_src

- Any tips?

*** Graphics
/A nice gimmick?/

**** Dot
A simple example [fn:15]

#+begin_src dot :file fig/dot-1.png
digraph G {
    rankdir=LR;
    subgraph cluster_c0 {a0 -> a1 -> a3;}
    subgraph cluster_c1 {b0 -> b1 -> b2 -> b3;}
    x -> a0;
    x -> b0;
    a1 -> b3;
    b1 -> a3;
}
#+end_src

#+RESULTS:
[[file:fig/dot-1.png]]

**** PlantUML
***** Setup
Load =plantuml= as =org-babel=-language:
#+begin_src elisp :eval no-export
(org-babel-do-load-languages
 'org-babel-load-languages
 '((plantuml . t)))
#+end_src

#+RESULTS:

Download .jar-file from http://plantuml.sourceforge.net/download.html
and set =org-plantuml-jar-path= accordingly:

#+begin_src elisp :results output :var p-path="/tmp/plantuml.jar" :var p-url="https://github.com/plantuml/plantuml/releases/download/v1.2022.1/plantuml-1.2022.1.jar" :results silent :eval no-export
(make-directory (file-name-directory p-path) :parents)
(url-copy-file p-url p-path)
(setq org-plantuml-jar-path p-path)
#+end_src

***** Usage of PlantUML
****** A Call Graph

#+begin_src plantuml :file fig/puml-1.png
skinparam monochrome true
Alice -> Bob: synchronous call
Alice ->> Bob: asynchronous call
#+end_src

#+RESULTS:
[[file:fig/puml-1.png]]

****** An Activity diagram

#+begin_src plantuml :file fig/puml-2.png
@startuml
skinparam monochrome true
title How to use org-babel
(*) --right--> "Create src block"
-right->[Execute] "Some results"
if "is satisfying?" then
-right->[no] "Modify src block"
-down->[Execute] "Some results"
else
-->[yes] (*)
endif
@enduml
#+end_src

#+RESULTS:
[[file:fig/puml-2.png]]

**** [Plotting Libary of Your Choice]
- Here we use [[https://matplotlib.org/][=matplotlib=]] to illustrate the point [fn:16]

#+begin_src python :results file :exports both :dir "./fig"
import numpy as np
import matplotlib.pyplot as plt
# Data for plotting
t = np.arange(0.0, 2.0, 0.01)
s = 1 + np.sin(2 * np.pi * t)

fig, ax = plt.subplots()
ax.plot(t, s)

ax.set(xlabel='Time (s)', ylabel='Voltage (mV)',
       title='Some sine wave')
ax.grid()

fig.savefig("sine.png")

return "sine.png"
#+end_src

#+RESULTS:
[[file:fig/sine.png]]

** Wrap-Up
- [ ] Use =org-babel= as a computing environment
  - configurtion
  - interaction
- [ ] Control its behaviour flexibly via various header args
  - pass results
  - use sessions
  - formulate dependencies
- [ ] Various execution "modes"
  - async
  - remote

** Discussion
*Your* use cases for =org-babel=?

*What* do you *like* most about it?

* Footnotes

[fn:1] https://en.wikisource.org/wiki/Bible_(King_James)/Genesis#11

[fn:2] https://orgmode.org/worg/org-contrib/babel/intro.html

[fn:3] https://orgmode.org/worg/org-contrib/babel/languages/index.html

[fn:4] https://orgmode.org/worg/org-contrib/babel/intro.html

[fn:5] https://orgmode.org/manual/Results-of-Evaluation.html

[fn:6] https://github.com/dfeich/org-babel-examples/blob/master/shell/shell-babel.org

[fn:7] https://www.cs.tufts.edu/~nr/noweb/

[fn:8] https://orgmode.org/worg/org-contrib/babel/languages/ob-doc-python.html

[fn:9] https://youtu.be/kkqVTDbfYp4?t=1605

[fn:10] https://orgmode.org/manual/The-Export-Dispatcher.html

[fn:11] https://git.codechem.com/halicea/dotfiles/-/blob/db793a9a23107ac2adf92e98bc10caead5495796/org/theme-readtheorg.setup

[fn:12] https://github.com/astahlman/ob-async/blob/9aac486073f5c356ada20e716571be33a350a982/ob-async.el#L161

[fn:13] https://gist.github.com/amake/3aa93948a19b82deb5b063939a57bfb0

[fn:14] https://emacs-lsp.github.io/lsp-mode/manual-language-docs/lsp-org/

[fn:15] https://github.com/dfeich/org-babel-examples/blob/master/graphviz/graphviz-babel.org

[fn:16] https://matplotlib.org/stable/gallery/lines_bars_and_markers/simple_plot.html

[fn:17] https://www.youtube.com/watch?v=dljNabciEGg
