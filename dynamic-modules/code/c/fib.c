#include <emacs-module.h>
#include <stdio.h>
#include <string.h>

int plugin_is_GPL_compatible;

static intmax_t fib_c (intmax_t n) {
	switch (n) {
	case 0: return 0;
	case 1: return 1;
	default:
		return fib_c(n-1) + fib_c(n-2);
	}
}

static emacs_value fib(emacs_env *env, ptrdiff_t nargs, emacs_value *args, void *data) {
	// argument count is checked by emacs
	emacs_value first_arg = args[0];
	intmax_t n = env->extract_integer(env, first_arg);

	// Error handling for extract_integer
	if (env->non_local_exit_check(env) != emacs_funcall_exit_return)
		return env->make_integer(env, 0);

	if (n < 0) {
		// call signal with the symbol 'error and a list containing the
		// error message as only entry.
		emacs_value list = env->intern(env, "list");
		emacs_value error_sym = env->intern(env, "error");
		char *err =  "Negative value";
		emacs_value err_msg = env->make_string(env, err, strlen(err));
		emacs_value err_data = env->funcall(env, list, 1, &err_msg);
		env->non_local_exit_signal(env, error_sym, err_data);
		return error_sym;
	}

	intmax_t res = fib_c(n);
	return env->make_integer(env, res);
}

int emacs_module_init(struct emacs_runtime *ert) {
	if (ert->size < sizeof(*ert))
		return 1;

	// register the function fib as fib-c
	emacs_env *env = ert->get_environment(ert);
	emacs_value func = env->make_function(env, 1, 1, fib, "Compute fib.", NULL);
	emacs_value symbol = env->intern(env, "fib-c");
	emacs_value args[] = { symbol, func };
	env->funcall(env, env->intern(env, "defalias"), 2, args);

	return 0;
}
