emacs::plugin_is_GPL_compatible!();

#[emacs::module]
fn init(_env: &emacs::Env) -> emacs::Result<()> {
    Ok(())
}

fn fib(n: i64) -> i64 {
    match n {
        0 => 0,
        1 => 1,
        _ => fib(n-1) + fib(n-2),
    }
}

#[emacs::defun]
fn fib_rs(_env: &emacs::Env, n: i64) -> emacs::Result<i64> {
    if n < 0 {
        failure::bail!("Negative argument");
    }
    Ok(fib(n))
}
