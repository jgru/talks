(defun fib-el (n)
  (case n
    (0 0)
    (1 1)
    (t (+ (fib-el (- n 1)) (fib-el (- n 2))))))
