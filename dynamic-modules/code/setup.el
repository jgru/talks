;; Emacs doesn't support unloading and reloading of modules. So the workaround
;; is to create a fresh .so file and load that every time.
(defun load-module (name)
  (let ((tmp (concat (make-temp-name "emod") ".so")))
    (copy-file name tmp)
    (load tmp)))
